var ApiService = (function () {
    var userList = [
        { id: 1, name: "User 1", birthDate: "10/08/1985" },
        { id: 2, name: "User 2", birthDate: "11/05/1986" },
        { id: 3, name: "User 3", birthDate: "01/01/1996" },
        { id: 4, name: "User 4", birthDate: "15/08/1995" },
    ];
    var bookList = [
        { isbn: "978-3-16-148410-0", title: "Libro numero 1", prezzo: 1350 },
        { isbn: "978-3-16-148410-1", title: "Libro numero 2", prezzo: 2000 },
        { isbn: "978-3-16-148410-2", title: "Libro numero 3", prezzo: 1050 },
        { isbn: "978-3-16-148410-3", title: "Libro numero 4", prezzo: 850 },
        { isbn: "978-3-16-148410-4", title: "Libro numero 5", prezzo: 1850 },
    ];
    var loanList = [
        {
            id: 1,
            user: 1,
            book: "978-3-16-148410-0",
            startLoan: "20/07/2020",
            endLoan: "20/08/2020",
        },
        {
            id: 2,
            user: 1,
            book: "978-3-16-148410-1",
            startLoan: "20/02/2020",
            endLoan: "20/05/2020",
        },
        {
            id: 3,
            user: 2,
            book: "978-3-16-148410-2",
            startLoan: "11/05/2019",
            endLoan: "11/06/2019",
        },
        {
            id: 4,
            user: 3,
            book: "978-3-16-148410-3",
            startLoan: "19/07/2020",
            endLoan: "19/09/2020",
        },
        {
            id: 5,
            user: 3,
            book: "978-3-16-148410-3",
            startLoan: "20/07/2020",
            endLoan: "20/09/2020",
        },
    ];

    function _buildPromise(data) {
        var promise = new Promise(function (resolve, reject) {
            setTimeout(function () {
                resolve(data);
            }, 1000);
        });

        return promise;
    }

    return {
        getUserList: function () {
            return _buildPromise(userList);
        },
        getBookList: function () {
            return _buildPromise(bookList);
        },
        getLoanList: function () {
            return _buildPromise(loanList);
        },
    };
})();
