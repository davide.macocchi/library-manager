var AppManager = (function () {
    const sectionInitialization = {
        book: SectionManagerBook.initialize,
        user: SectionManagerUser.initialize,
        loan: SectionManagerLoan.initialize,
    };

    function initialize() {
        console.log("initialize - start");

        //menu listener
        $(".navigation__item").on("click", initializeSectionHandler);

        //first default listener
        initializeSection("user");

        console.log("initialize - end");
    }

    function initializeSectionHandler() {
        var section = $(this).data("section");
        if (section) {
            initializeSection(section);
        }
    }
    function initializeSection(section) {
        if (sectionInitialization[section]) {
            $(".page-container").children().hide();
            sectionInitialization[section]();
        }
    }

    return {
        initialize: initialize,
    };
})();

AppManager.initialize();
