var SectionManagerBook = (function () {
    function initialize() {
        console.log("initialize book section");

        ApiService.getBookList().then(
            function (bookList) {
                if (
                    bookList &&
                    Array.isArray(bookList) &&
                    bookList.length > 0
                ) {
                    var html = TableUtils.buildHtml(
                        ["ISBN", "Titolo", "Prezzo"],
                        bookList.map(function (item) {
                            return [item.isbn, item.title, item.prezzo];
                        })
                    );

                    $(".page-container .book-page").html(html).show();
                }
            },
            function () {
                console.log("errore nella retrieve dei dati");
            }
        );
    }

    return {
        initialize: initialize,
    };
})();
