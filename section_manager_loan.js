var SectionManagerLoan = (function () {
    function initialize() {
        console.log("initialize loan section");

        $.when(
            ApiService.getUserList(),
            ApiService.getBookList(),
            ApiService.getLoanList()
        ).then(
            function (userList, bookList, loanList) {
                if (
                    loanList &&
                    Array.isArray(loanList) &&
                    loanList.length > 0
                ) {
                    var values = loanList.map(function (loan) {
                        loan.userData = userList.find(function (user) {
                            return user.id === loan.user;
                        });

                        loan.bookData = bookList.find(function (book) {
                            return book.isbn === loan.book;
                        });
                        return [
                            loan.userData ? loan.userData.name : "-",
                            loan.bookData ? loan.bookData.isbn : "-",
                            loan.bookData ? loan.bookData.title : "-",
                            loan.startLoan,
                            loan.endLoan,
                        ];
                    });
                    var html = TableUtils.buildHtml(
                        ["Nome", "ISBN", "Titolo", "Inizio", "Fine"],
                        values
                    );

                    $(".page-container .loan-page").html(html).show();
                }
            },
            function () {
                console.log("errore");
            }
        );
    }

    return {
        initialize: initialize,
    };
})();
