var SectionManagerUser = (function () {
    function initialize() {
        console.log("initialize user section");

        ApiService.getUserList().then(
            function (userList) {
                if (userList && Array.isArray(userList) && userList.length) {
                    var html = TableUtils.buildHtml(
                        ["Nome", "Data Nascita"],
                        userList.map(function (item) {
                            return [item.name, item.birthDate];
                        })
                    );

                    $(".page-container .user-page").html(html).show();
                }
            },
            function () {
                console.log("errore nella retrieve dei dati");
            }
        );
    }

    return {
        initialize: initialize,
    };
})();
