var TableUtils = (function () {
    function buildHtml(columnArray, itemArray) {
        console.log(columnArray, itemArray);
        var html = "";
        try {
            html += '<table width="100%">';

            html += "<tr>";
            html += columnArray
                .map(function (i) {
                    return "<th>" + i + "</th>";
                })
                .join("");
            html += "</tr>";

            html += itemArray
                .map((item) => {
                    return (
                        "<tr>" +
                        item
                            .map(function (i) {
                                return "<td>" + i + "</td>";
                            })
                            .join("") +
                        "</tr>"
                    );
                })
                .join("");

            html += "</table>";
        } catch (error) {
            console.log("errore nel generare la table", error);
            html = "";
        }
        return html;
    }

    return {
        buildHtml: buildHtml,
    };
})();
